import { Article } from "./article";

export interface AricleService{
    getArticles(): Promise<Article[]>;
    deleteArticle(id: number): any;
    updateArticle(article: Article): any;
}